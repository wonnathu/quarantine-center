<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    protected $fillable = [
        'center_name',
        'township_id',
    ];

    public function township()
    {
        return $this->belongsTo('App\Township', 'township_id');
    }

    public function rooms()
    {
        return $this->hasMany('App\Room', 'center_id');
    }

    public function quarantineUsers()
    {
        return $this->hasMany('App\QuarantineUser', 'center_id');
    }

    public function volunteers()
    {
        return $this->hasMany('App\Volunteer', 'center_id');
    }
}
