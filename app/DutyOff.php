<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DutyOff extends Model
{
    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'start_date',
        'end_date',
        'volunteer_id',
    ];

    public function volunteer()
    {
        return $this->belongsTo('App\Volunteer', 'volunteer_id');
    }
}
