<?php

namespace App\Http\Controllers;

use App\Center;
use App\Http\Requests\CenterRequest;
use App\Http\Requests\CenterUpdateRequest;
use App\Township;
use Illuminate\Http\Request;

class CenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centers = Center::all();
        return view('dashboard.center.index', [
            'centers' => $centers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $townships = Township::orderBy('township_name', 'asc')->get();
        return view('dashboard.center.create', [
            'townships' => $townships,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CenterRequest $request)
    {
        $center = Center::create([
            'center_name' => $request->center_name,
            'township_id' => $request->township_id,
        ]);

        $township_name = $center->township->township_name;

        return redirect()->route('centers.index')->with('store', "Center name is $center->center_name. Township name is $township_name, Stored!!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show(Center $center)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit(Center $center)
    {
        $townships = Township::orderBy('township_name', 'asc')->get();
        return view('dashboard.center.edit', [
            'center' => $center,
            'townships' => $townships,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(CenterUpdateRequest $request, Center $center)
    {

        if ($request->center_name == $center->center_name) {
            $center->update([
                'center_name' => $request->center_name,
                'township_id' => $request->township_id,
            ]);
        } else {
            $request->validate([
                'center_name' => 'required|unique:centers|max:255',
                'township_id' => 'required|integer',
            ]);

            $center->update([
                'center_name' => $request->center_name,
                'township_id' => $request->township_id,
            ]);
        }

        $township_name = $center->township->township_name;

        return redirect()->route('centers.index')->with('update', "Center name is $center->center_name. Township name is $township_name, Updated!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy(Center $center)
    {
        $center->delete();
        $township_name = $center->township->township_name;

        return redirect()->route('centers.index')->with('delete', "Center name is $center->center_name. Township name is $township_name, Deleted!!!");
    }
}
