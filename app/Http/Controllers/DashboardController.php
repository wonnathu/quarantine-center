<?php

namespace App\Http\Controllers;

use App\Center;
use App\QuarantineUser;
use App\Room;
use App\State;
use App\Township;
use App\Volunteer;

class DashboardController extends Controller
{
    public function index()
    {
        $states = State::all();

        $township = Township::all();
        $center = Center::all();
        $room = Room::all();
        $patient = QuarantineUser::all();
        $volunteer = Volunteer::all();

        return view('dashboard.index', [
            'states' => $states,
            'township' => count($township),
            'center' => count($center),
            'room' => count($room),
            'patient' => count($patient),
            'volunteer' => count($volunteer),
        ]);
    }

    public function state($state)
    {
        $townships = Township::where('state_id', $state)->get();

        $state = State::find($state);

        return view('dashboard.township', [
            'state' => $state,
            'townships' => $townships,
        ]);
    }

    public function township($township)
    {
        $centers = Center::where('township_id', $township)->get();

        $township = Township::find($township);

        return view('dashboard.center', [
            'centers' => $centers,
            'township' => $township,
        ]);

    }

    public function center($center)
    {
        $rooms = Room::where('center_id', $center)->get();

        $volunteers = Volunteer::where('center_id', $center)->get();

        $patients = QuarantineUser::where('center_id', $center)->get();

        $center = Center::find($center);

        return view('dashboard.singleCenter', [
            'rooms' => $rooms,
            'volunteers' => $volunteers,
            'center' => $center,
            'patients' => $patients,
        ]);
    }

    public function patient($center)
    {
        $quarantine_patients = QuarantineUser::FilterOn()->where('center_id', $center)->get();

        $center = Center::find($center);
        return view('dashboard.singlePatient', [
            'quarantine_patients' => $quarantine_patients,
            'center' => $center,
        ]);
    }
}
