<?php

namespace App\Http\Controllers;

use App\DutyOff;
use App\Http\Requests\DutyOffRequest;
use App\Volunteer;
use Illuminate\Http\Request;

class DutyOffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $volunteers = Volunteer::all();
        return view('dashboard.dutyoff.index', [
            'volunteers' => $volunteers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $volunteers = Volunteer::orderBy('volunteer_name', 'asc')->get();
        return view('dashboard.dutyoff.create', [
            'volunteers' => $volunteers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DutyOffRequest $request)
    {
        $dutyoff = DutyOff::create([
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'volunteer_id' => $request->volunteer_id,
        ]);

        $volunteer = $dutyoff->volunteer->volunteer_name;

        return back()->with('store', "$volunteer Added new On Duty Off");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DutyOff  $dutyOff
     * @return \Illuminate\Http\Response
     */
    public function show(DutyOff $dutyOff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DutyOff  $dutyOff
     * @return \Illuminate\Http\Response
     */
    public function edit(DutyOff $dutyOff)
    {

        $volunteers = Volunteer::orderBy('volunteer_name', 'asc')->get();
        return view('dashboard.dutyoff.edit', [
            'volunteers' => $volunteers,
            'dutyOff' => $dutyOff,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DutyOff  $dutyOff
     * @return \Illuminate\Http\Response
     */
    public function update(DutyOffRequest $request, DutyOff $dutyOff)
    {
        $dutyOff->update([
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'volunteer_id' => $request->volunteer_id,
        ]);
        return redirect()->route('volunteers.show', $dutyOff->volunteer_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DutyOff  $dutyOff
     * @return \Illuminate\Http\Response
     */
    public function destroy(DutyOff $dutyOff)
    {
        $dutyOff->delete();
        return redirect()->route('volunteers.show', $dutyOff->volunteer_id);
    }
}
