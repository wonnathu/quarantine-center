<?php

namespace App\Http\Controllers;

use App\Http\Requests\OndutyRequest;
use App\Onduty;
use App\Volunteer;
use Illuminate\Http\Request;

class OndutyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $volunteers = Volunteer::all();
        return view('dashboard.onduty.index', [
            'volunteers' => $volunteers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $volunteers = Volunteer::orderBy('volunteer_name', 'asc')->get();
        return view('dashboard.onduty.create', [
            'volunteers' => $volunteers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OndutyRequest $request)
    {
        $onduty = Onduty::create([
            'entry_date' => $request->entry_date,
            'leave_date' => $request->leave_date,
            'volunteer_id' => $request->volunteer_id,
        ]);

        $volunteer = $onduty->volunteer->volunteer_name;

        return back()->with('store', "$volunteer Added new On Duty");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Onduty  $onduty
     * @return \Illuminate\Http\Response
     */
    public function show(Onduty $onduty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Onduty  $onduty
     * @return \Illuminate\Http\Response
     */
    public function edit(Onduty $onduty)
    {
        $volunteers = Volunteer::orderBy('volunteer_name', 'asc')->get();
        return view('dashboard.onduty.edit', [
            'volunteers' => $volunteers,
            'onduty' => $onduty,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Onduty  $onduty
     * @return \Illuminate\Http\Response
     */
    public function update(OndutyRequest $request, Onduty $onduty)
    {
        $onduty->update([
            'entry_date' => $request->entry_date,
            'leave_date' => $request->leave_date,
            'volunteer_id' => $request->volunteer_id,
        ]);
        return redirect()->route('volunteers.show', $onduty->volunteer_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Onduty  $onduty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Onduty $onduty)
    {
        $onduty->delete();
        return redirect()->route('volunteers.show', $onduty->volunteer_id);
    }
}
