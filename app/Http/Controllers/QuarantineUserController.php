<?php

namespace App\Http\Controllers;

use App\Center;
use App\Http\Requests\PatientRequest;
use App\QuarantineUser;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuarantineUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quarantine_patients = QuarantineUser::all();
        return view('dashboard.quser.index', [
            'quarantine_patients' => $quarantine_patients,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centers = Center::orderBy('center_name', 'asc')->get();
        //$centers = Center::all();
        $rooms = Room::orderBy('room_name', 'asc')->get();
        return view('dashboard.quser.create', [
            'centers' => $centers,
            'rooms' => $rooms,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
        //dd($request->all());

        $path = "";
        if ($request->image) {
            $path = $request->file('image')->store('public/image');
        } else {
            $path = "public/image/user.png";
        }

        $quser = new QuarantineUser();
        $quser->quser_name = $request->quser_name;
        $quser->profile_image = $path;
        $quser->phone = $request->phone;
        $quser->address = $request->address;
        $quser->entry_date = $request->entry_date;
        $quser->leave_date = $request->leave_date;
        $quser->remark = $request->remark;
        $quser->center_id = $request->center_id;
        $quser->room_id = $request->room_id;
        $quser->save();

        /*$quarantine_patient = QuarantineUser::create([
        'quser_name' => $request->quser_name,
        'profile_image' => $path,
        'phone' => $request->phone,
        'address' => $request->address,
        'entry_date' => $request->entry_date,
        'leave_date' => $request->leave_date,
        'remark' => $request->remark,
        'center_id' => $request->center_id,
        'room_id' => $request->room_id,
        ]);*/

        return redirect()->route('quarantineUser.index')->with('store', " $quser->quser_name's information Stored!!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuarantineUser  $quarantineUser
     * @return \Illuminate\Http\Response
     */
    public function show(QuarantineUser $quarantineUser)
    {
        return view('dashboard.quser.show', [
            'quarantineUser' => $quarantineUser,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuarantineUser  $quarantineUser
     * @return \Illuminate\Http\Response
     */
    public function edit(QuarantineUser $quarantineUser)
    {
        // dd($quarantineUser);
        $centers = Center::orderBy('center_name', 'asc')->get();
        $rooms = Room::orderBy('room_name', 'asc')->get();
        return view('dashboard.quser.edit', [
            'quarantineUser' => $quarantineUser,
            'centers' => $centers,
            'rooms' => $rooms,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuarantineUser  $quarantineUser
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, QuarantineUser $quarantineUser)
    {
        if ($request->image) {
            $path = $request->file('image')->store('public/image');
            $quarantineUser->profile_image = $path;
        }

        $quarantineUser->quser_name = $request->quser_name;
        $quarantineUser->phone = $request->phone;
        $quarantineUser->address = $request->address;
        $quarantineUser->entry_date = $request->entry_date;
        $quarantineUser->leave_date = $request->leave_date;
        $quarantineUser->remark = $request->remark;
        $quarantineUser->center_id = $request->center_id;
        $quarantineUser->room_id = $request->room_id;
        $quarantineUser->update();

        return redirect()->route('quarantineUser.index')->with('update', " $quarantineUser->quser_name's information is Updated!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuarantineUser  $quarantineUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuarantineUser $quarantineUser)
    {
        $quarantineUser->delete();
        return redirect()->route('quarantineUser.index')->with('delete', " $quarantineUser->quser_name's information is Deleted!!!");

    }
}
