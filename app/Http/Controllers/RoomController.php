<?php

namespace App\Http\Controllers;

use App\Center;
use App\Http\Requests\RoomRequest;
use App\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all();
        return view('dashboard.room.index', [
            'rooms' => $rooms,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centers = Center::orderBy('center_name', 'asc')->get();
        return view('dashboard.room.create', [
            'centers' => $centers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomRequest $request)
    {
        $room = Room::create([
            'room_name' => $request->room_name,
            'center_id' => $request->center_id,
        ]);

        $center_name = $room->center->center_name;
        return redirect()->route('rooms.index')->with('store', "Room Name is $room->room_name. Center name is $center_name, Stored!!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $centers = Center::orderBy('center_name', 'asc')->get();
        return view('dashboard.room.edit', [
            'centers' => $centers,
            'room' => $room,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(RoomRequest $request, Room $room)
    {
        $room->update([
            'room_name' => $request->room_name,
            'center_id' => $request->center_id,
        ]);

        $center_name = $room->center->center_name;
        return redirect()->route('rooms.index')->with('update', "Room Name is $room->room_name. Center name is $center_name, Updated!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();

        $center_name = $room->center->center_name;
        return redirect()->route('rooms.index')->with('delete', "Room Name is $room->room_name. Center name is $center_name, Deleted!!!");
    }
}
