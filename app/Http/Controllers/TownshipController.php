<?php

namespace App\Http\Controllers;

use App\Http\Requests\TownshipRequest;
use App\State;
use App\Township;
use Illuminate\Http\Request;

class TownshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $townships = Township::all();
        return view('dashboard.township.index', [
            'townships' => $townships,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::orderBy('state_name', 'asc')->get();
        return view('dashboard.township.create', [
            'states' => $states,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TownshipRequest $request)
    {
        $township = Township::create([
            'township_name' => $request->township_name,
            'state_id' => $request->state_id,
        ]);
        $state_name = $township->state->state_name;
        return redirect()->route('townships.index')->with('store', "Township is $township->township_name. Region or State Name is $state_name, Stored!!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function show(Township $township)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function edit(Township $township)
    {
        $states = State::orderBy('state_name', 'asc')->get();
        return view('dashboard.township.edit', [
            'township' => $township,
            'states' => $states,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function update(TownshipRequest $request, Township $township)
    {
        $township->update([
            'township_name' => $request->township_name,
            'state_id' => $request->state_id,
        ]);

        $state_name = $township->state->state_name;

        return redirect()->route('townships.index')->with('update', "Township is $township->township_name. Region or State Name is $state_name, Updated!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function destroy(Township $township)
    {
        $township->delete();
        $state_name = $township->state->state_name;

        return redirect()->route('townships.index')->with('delete', "Township is $township->township_name. Region or State Name is $state_name, Deleted!!!");

    }
}
