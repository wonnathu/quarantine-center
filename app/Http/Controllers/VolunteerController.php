<?php

namespace App\Http\Controllers;

use App\Center;
use App\Http\Requests\VolunteerRequest;
use App\Volunteer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $volunteers = Volunteer::all();
        return view('dashboard.volunteer.index', [
            'volunteers' => $volunteers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centers = Center::orderBy('center_name', 'asc')->get();
        return view('dashboard.volunteer.create', [
            'centers' => $centers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VolunteerRequest $request)
    {

        $path = "";
        if ($request->image) {
            $path = $request->file('image')->store('public/image');
        } else {
            $path = "public/image/user.png";
        }

        $volunteer = new Volunteer();
        $volunteer->volunteer_name = $request->volunteer_name;
        $volunteer->profile_image = $path;
        $volunteer->phone = $request->phone;
        $volunteer->address = $request->address;
        $volunteer->center_id = $request->center_id;
        $volunteer->save();

        return redirect()->route('volunteers.index')->with('store', "$volunteer->volunteer_name 's information Stored!!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\volunteer  $volunteer
     * @return \Illuminate\Http\Response
     */
    public function show(volunteer $volunteer)
    {
        return view('dashboard.volunteer.show', [
            'volunteer' => $volunteer,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\volunteer  $volunteer
     * @return \Illuminate\Http\Response
     */
    public function edit(volunteer $volunteer)
    {
        $centers = Center::orderBy('center_name', 'asc')->get();
        return view('dashboard.volunteer.edit', [
            'centers' => $centers,
            'volunteer' => $volunteer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\volunteer  $volunteer
     * @return \Illuminate\Http\Response
     */
    public function update(VolunteerRequest $request, volunteer $volunteer)
    {
        if ($request->image) {
            $path = $request->file('image')->store('public/image');
            $volunteer->profile_image = $path;
        }

        $volunteer->volunteer_name = $request->volunteer_name;
        $volunteer->phone = $request->phone;
        $volunteer->address = $request->address;
        $volunteer->center_id = $request->center_id;
        $volunteer->update();

        return redirect()->route('volunteers.index')->with('update', "$volunteer->volunteer_name 's information Updated!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\volunteer  $volunteer
     * @return \Illuminate\Http\Response
     */
    public function destroy(volunteer $volunteer)
    {
        $volunteer->delete();
        return redirect()->route('volunteers.index')->with('delete', "$volunteer->volunteer_name 's information Delete!!!");
    }
}
