<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quser_name' => [
                'required',
                'string',
                'max:255',
            ],

            'phone' => [
                'nullable',
                'string',
            ],
            'address' => [
                'required',
                'string',
            ],
            'entry_date' => [
                'required',
            ],
            'leave_date' => [
                'required',
            ],
            'remark' => [
                'nullable',
                'string',
            ],
            'center_id' => [
                'required',
                'integer',
            ],
            'room_id' => [
                'required',
                'integer',
            ],
            'image' => [
                'nullable',
                'image',
            ],
        ];
    }
}
