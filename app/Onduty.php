<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Onduty extends Model
{
    protected $dates = [
        'entry_date',
        'leave_date',
    ];

    protected $fillable = [
        'entry_date',
        'leave_date',
        'volunteer_id',
    ];
    public function volunteer()
    {
        return $this->belongsTo('App\Volunteer', 'volunteer_id');
    }
}
