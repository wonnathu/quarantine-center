<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuarantineUser extends Model
{
    protected $dates = [
        'entry_date',
        'leave_date',
    ];
    protected $fillable = [
        'quser_name',
        'image',
        'phone',
        'address',
        'entry_date',
        'leave_date',
        'remark',
        'center_id',
        'room_id',
    ];

    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id');
    }

    public function center()
    {
        return $this->belongsTo('App\Center', 'center_id');
    }

    public function scopeFilterOn($query)
    {
        if (request('room_id')) {
            return $query->where('room_id', request('room_id'));
        }
        // elseif(request('status') == 'trash'){
        //     $query->onlyTrashed();
        // }
    }

}
