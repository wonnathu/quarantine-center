<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'room_name',
        'center_id',
    ];

    public function center()
    {
        return $this->belongsTo('App\Center', 'center_id');
    }

    public function quarantineUsers()
    {
        return $this->hasMany('App\QuarantineUser', 'room_id');
    }
}
