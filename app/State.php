<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['state_name'];

    public function townships()
    {
        return $this->hasMany('App\Township', 'state_id');
    }
}
