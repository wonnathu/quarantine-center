<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Township extends Model
{
    protected $fillable = [
        'township_name',
        'state_id',
    ];

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    public function centers()
    {
        return $this->hasMany('App\Center', 'center_id');
    }
}
