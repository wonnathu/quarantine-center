<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{

    protected $fillable = [
        'volunteer_name',
        'phone',
        'address',
        'image',
        'center_id',
    ];

    public function center()
    {
        return $this->belongsTo('App\Center', 'center_id');
    }

    public function ondutys()
    {
        return $this->hasMany('App\Onduty', 'volunteer_id');
    }

    public function dutyoffs()
    {
        return $this->hasMany('App\DutyOff', 'volunteer_id');
    }
}
