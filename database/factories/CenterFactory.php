<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Center;
use App\Township;
use Faker\Generator as Faker;

$factory->define(Center::class, function (Faker $faker) {
    $township = Township::all();
    $township_id = $township->random()->id;
    return [
        'center_name' => $faker->streetName,
        'township_id' => $township_id,
    ];
});
