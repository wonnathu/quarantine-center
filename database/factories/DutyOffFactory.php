<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DutyOff;
use Faker\Generator as Faker;

$factory->define(DutyOff::class, function (Faker $faker) {
    return [
        'start_date' => now(),
        'end_date' => now()->addDays(21),
        // 'volunteer_id' => factory(App\Volunteer::class),
        'volunteer_id' => 1,
    ];
});
