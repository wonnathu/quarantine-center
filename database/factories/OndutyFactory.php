<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Onduty;
use Faker\Generator as Faker;

$factory->define(Onduty::class, function (Faker $faker) {
    // $volunteer = Volunteer::all();
    // $volunteer_id = $volunteer->id;
    return [
        'entry_date' => now(),
        'leave_date' => now()->addDays(21),
        //'volunteer_id' => factory(App\Volunteer::class),
        'volunteer_id' => 1,
    ];
});
