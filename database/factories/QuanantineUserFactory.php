<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\QuarantineUser;
use App\Room;
use Faker\Generator as Faker;

$factory->define(QuarantineUser::class, function (Faker $faker) {
    $rooms = Room::all();
    $room_id = $rooms->random()->id;

    $room = Room::find($room_id);
    $center_id = $room->center_id;

    return [
        'quser_name' => $faker->name,
        'profile_image' => 'public/image/user.png',
        'phone' => '959' . $faker->unique()->numberBetween($min = 100000000, $max = 999999999),
        'address' => $faker->address,
        'entry_date' => now(),
        'leave_date' => now()->addDays(21),
        'remark' => 'nothing',
        'room_id' => $room_id,
        'center_id' => $center_id,
    ];
});
