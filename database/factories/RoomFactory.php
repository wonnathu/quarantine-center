<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Center;
use App\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    $center = Center::all();
    $center_id = $center->random()->id;
    return [
        'room_name' => 'A' . rand(1, 10),
        'center_id' => $center_id,
    ];
});
