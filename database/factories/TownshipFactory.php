<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\State;
use App\Township;
use Faker\Generator as Faker;

$factory->define(Township::class, function (Faker $faker) {
    $state = State::all();
    $state_id = $state->random()->id;
    return [
        'township_name' => $faker->city,
        'state_id' => $state_id,
    ];
});
