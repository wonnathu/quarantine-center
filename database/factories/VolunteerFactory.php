<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Center;
use App\Volunteer;
use Faker\Generator as Faker;

$factory->define(Volunteer::class, function (Faker $faker) {
    $center = Center::all();
    $center_id = $center->random()->id;
    return [
        'volunteer_name' => $faker->name,
        'profile_image' => 'public/image/user.png',
        'phone' => '959' . $faker->unique()->numberBetween($min = 100000000, $max = 999999999),
        'address' => $faker->address,
        'center_id' => $center_id,
    ];
});
