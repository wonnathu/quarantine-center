<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuarantineUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quarantine_users', function (Blueprint $table) {
            $table->id();
            $table->string('quser_name');
            $table->string('profile_image')->nullable();
            $table->string('phone')->nullable();
            $table->text('address');
            $table->date('entry_date');
            $table->date('leave_date');
            $table->text('remark')->nullable();
            $table->string('room_id');
            $table->string('center_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quarantine_users');
    }
}
