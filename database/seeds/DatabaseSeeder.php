<?php

use App\State;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        //admin
        $users = factory(App\User::class, 1)->create([
            'name' => 'Dr.WonnaThu',
            'email' => 'admin@admin.com',
        ]);

        //state
        //$state = factory(App\State::class, 5)->create();
        //$states = ['YGN', 'MDY', 'NPT', 'SHAN', 'SG'];
        $states = ['YGN'];
        foreach ($states as $state) {
            $state_table = new State();
            $state_table->state_name = $state;
            $state_table->save();
        }

        //township
        $township = factory(App\Township::class, 2)->create();

        //center
        $center = factory(App\Center::class, 5)->create();

        //room
        $room = factory(App\Room::class, 50)->create();

        //quantine user
        $quarantine_user = factory(App\QuarantineUser::class, 100)->create();

        //volunteer
        //$volunteers = factory(App\Volunteer::class, 10)->create();
        $volunteers = factory(App\Volunteer::class, 25)
            ->create()
            ->each(function ($volunteer) {
                $volunteer->ondutys()->save(factory(App\Onduty::class)->make());
            })->each(function ($volunteer) {
            $volunteer->dutyoffs()->save(factory(App\DutyOff::class)->make());
        });

        //Onduty Volunteer
        //$volunteer_onduty = factory(App\Onduty::class, 25)->create();

        //Duty Off Volunteer
        //$volunteer_onduty = factory(App\DutyOff::class, 25)->create();
    }
}
