@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Add New Quarantine Center
        </div>
        <div class="page-title-actions">
            <a href="{{route('centers.index')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-back font-size-xl "> </i> Back To Center
            </a>
        </div>
    </div>
  </div>

      <div class="col-md-8 offset-md-2  border p-5 mt-5 bg-light">
          <form action="{{route('centers.store')}}" method="post">
            @csrf
          <div class="form-group">
            <label for="center_name" class="sr-only">Add New Center</label>
            <input type="text" class="form-control @error('center_name') border border-danger @enderror" id="center_name" placeholder="Enter Quarantine Name" name="center_name">
            @error('center_name')
            <small id="emailHelp" class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-group">
            <label for="township_id">Choose Township</label>
            <select class="form-control" id="township_id" name="township_id">
              @foreach($townships as $township)
              <option value="{{$township->id}}">{{$township->township_name}}</option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">
            Add New Center
          </button>
        </form>
      </div>

@endsection
