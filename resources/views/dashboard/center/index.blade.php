@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>
                Quarantine Center Lists
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{route('centers.create')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-note font-size-xl "> </i> Add New Center
            </a>
        </div>
    </div>
</div>

@if (session('store'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('store')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('update'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('update')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('delete'))
<div class="alert alert-danger mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('delete')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<table id="center" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th style="width: 100px">ID</th>
        <th>Quarantine Center Names</th>
        <th>Townships</th>
        <th>State / Region</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($centers as $center)
        <tr>
            <td>{{$center->id}}</td>
            <td>{{$center->center_name}}</td>
            <td>{{$center->township->township_name}}</td>
            <td>{{$center->township->state->state_name}}</td>
            <td>
                <a href="{{route('centers.edit', $center->id)}}" class="ml-3 text-info font-size-lg" title="Edit center">
                    <i class="pe-7s-note2"> </i>
                </a>
                <form action="{{ route('centers.destroy', $center->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete center">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th style="width: 100px">ID</th>
        <th>Quarantine Center Names</th>
        <th>centers</th>
        <th>State / Region</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>

  @section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#center').DataTable();
    });
  </script>
  @endsection

  @endsection
