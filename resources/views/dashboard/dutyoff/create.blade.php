@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Add New Duty Off For Volunteers
        </div>
    </div>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
  @endif

  @if (session('store'))
    <div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
      <strong>{{session('store')}}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif


      <div class="col-md-8 offset-md-2  border p-5 mt-5 mb-5 bg-light">
          <form action="{{route('dutyOffs.store')}}" method="post" >
            @csrf

            <div class="form-row">
            <div class="col-md-6 mb-3">
              <label for="start_date">Start Date *</label>
              <input type="date" class="form-control @error('start_date') border border-danger @enderror" id="start_date" name="start_date">
              @error("start_date")
              <small  class="mt-3 form-text text-danger">
                {{ $message }}
              </small>
              @enderror
            </div>
            <div class="col-md-6 mb-3">
              <label for="end_date">End Date *</label>
              <input type="date" class="form-control @error('end_date') border border-danger @enderror" id="end_date" name="end_date">
              @error("end_date")
              <small  class="mt-3 form-text text-danger">
                {{ $message }}
              </small>
              @enderror
            </div>
          </div>

          <div class="form-group">
            <label for="volunteer_id">Choose Volunteer</label>
            <select class="form-control" name="volunteer_id">
              @foreach($volunteers as $volunteer)
              <option value="{{$volunteer->id}}">
                {{$volunteer->volunteer_name}}
              </option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">
            Add New Duty Off
          </button>
        </form>
      </div>

@endsection
