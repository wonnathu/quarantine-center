@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
                Quarantine Center Management System (Dashboard)
        </div>
    </div>
</div>

<div class='row'>
    <div class="col-md-5">
        <div class="card h-100" >
            <div class="card-body">
                <!-- <h5 class="text-center">Quarantine Management System</h5> -->
                <div class="row mt-3">
                    <div class="col-6 pt-3">
                        <h2 class="card-title text-center">Patients</h2>
                        <p class="h2 text-center text-success">{{$patient}}</p>
                    </div>
                    <div class="col-6 pt-5">
                        <h2 class="card-title text-center">Volunteers</h2>
                        <p class="h2 text-center text-info">{{$volunteer}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-6 mb-3 mt-3">
                <div class="card bg-night-fade" >
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <h5 class="card-title">Region/State</h5>
                            </div>
                            <div class="float-right">
                                <p class="h2 text-light">{{count($states)}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3 mt-3">
                <div class="card bg-arielle-smile" >
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <h5 class="card-title">Townships</h5>
                            </div>
                            <div class="float-right">
                                <p class="h2 text-light">{{$township}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3 mt-3">
                <div class="card bg-happy-green" >
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <h5 class="card-title">Q Centers</h5>
                            </div>
                            <div class="float-right">
                                <p class="h2 text-light">{{$center}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3 mt-3">
                <div class="card bg-strong-bliss" >
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <h5 class="card-title">Rooms</h5>
                            </div>
                            <div class="float-right">
                                <p class="h2 text-light">{{$room}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider mt-5 mb-5" ></div>

<div class="mb-5">
    <h2 class="text-center">
        Region / State
    </h2>

    <div class="row mt-5">
        @foreach($states as $state)
        <div class="col-md-3 mb-4">
            <div class="card border-info h-100">
                <div class="card-body p-0">
                    <a href="{{route('state', $state->id)}}" class="h4 m-0 p-4 d-block text-center" style="text-decoration: none;">
                        <i class="metismenu-icon h3 text-info pe-7s-network"></i>
                        <h4 class="text-muted h5">
                            {{$state->state_name}}
                        </h4>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</div>

@endsection
