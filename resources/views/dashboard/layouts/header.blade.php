<div class="app-header header-shadow bg-light header-text-dark">
  <div class="app-header__logo">
      <div class="logo-src"></div>
      <div class="header__pane ml-auto">
          <div>
              <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                  <span class="hamburger-box">
                      <span class="hamburger-inner"></span>
                  </span>
              </button>
          </div>
      </div>
  </div>
  <div class="app-header__mobile-menu">
      <div>
          <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
              <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
              </span>
          </button>
      </div>
  </div>
  <div class="app-header__menu">
      <span>
          <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
              <span class="btn-icon-wrapper">
                  <i class="fa fa-ellipsis-v fa-w-6"></i>
              </span>
          </button>
      </span>
  </div>
  <div class="app-header__content">
    <div class="app-header-right">
      <!-- <div class="search-wrapper">
          <div class="input-holder">
              <input type="text" class="search-input" placeholder="Type to search">
              <button class="search-icon"><span></span></button>
          </div>
          <button class="close"></button>
      </div> -->
      <ul class="header-menu nav">
          <!-- <li class="nav-item">
              <a href="javascript:void(0);" class="nav-link">
                  <i class="nav-link-icon fa fa-database"> </i>
                  Statistics
              </a>
          </li>
          <li class="btn-group nav-item">
              <a href="javascript:void(0);" class="nav-link">
                  <i class="nav-link-icon fa fa-edit"></i>
                  Projects
              </a>
          </li> -->
          <li class="dropdown nav-item">
              <span class="nav-link">
              {{ Auth::user()->name }}
              </span>
          </li>
          <li class="dropdown nav-item">
              <a href="#" class="nav-link">
                  <i class="nav-link-icon fa fa-cog"></i>
                  Settings
              </a>
          </li>
          <li class="dropdown nav-item">
                <a class="nav-link"
                    href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                >
                  <i class="nav-link-icon fas fa-sign-out-alt"></i>
                  {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
          </li>
      </ul>
    </div>
    <!-- <div class="app-header-right">
      <div class="header-btn-lg pr-0">
          <div class="widget-content p-0">
              <div class="widget-content-wrapper">
                  <div class="widget-content-left">
                      <span class="mr-2 text-primary">
                      {{ Auth::user()->name }}
                      </span>
                      <a href=""><i class="pe-7s-next-2"> </i> logout</a>
                      <div class="btn-group">
                          <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                              <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                              <i class="fa fa-angle-down ml-2 opacity-8"></i>
                          </a>
                          <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                              <button type="button" tabindex="0" class="dropdown-item">User Account</button>
                              <button type="button" tabindex="0" class="dropdown-item">Settings</button>
                              <div tabindex="-1" class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div> -->
  </div>
</div>
