<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>@yield("title")</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Description Content">
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Architech Ui CSS -->
    <link href="{{asset('main.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
<body>
    <div id="app" class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        @include('dashboard.layouts.header')
        @include('dashboard.layouts.settings')

        <div class="app-main bg-light">
                @include('dashboard.layouts.sidebar')
                <div class="app-main__outer">
                    <div class="app-main__inner border">
                        @yield('content')
                    </div>
                    @include('dashboard.layouts.footer')
                </div>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/scripts/main.js')}}"></script>
    </body>
    @yield('script')
</html>
