<div class="app-sidebar sidebar-shadow bg-night-sky sidebar-text-light">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="{{route('dashboard')}}" class="{{Request::path() === 'dashboard' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Admin Panel</li>
                <li>
                    <a href="{{route('states.index')}}" class="{{Request::path() === 'states' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-network"></i>
                        Region / State
                    </a>
                </li>
                <!-- <li class="app-sidebar__heading">TownShips</li> -->
                <li>
                    <a href="{{route('townships.index')}}" class="{{Request::path() === 'townships' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-culture"></i>
                        Townships
                    </a>
                </li>
                <!-- <li class="app-sidebar__heading">Quarantine Center</li> -->
                <li>
                    <a href="{{route('centers.index')}}" class="{{Request::path() === 'centers' ? 'mm-active' : '' }}" >
                        <i class="metismenu-icon pe-7s-id"></i>
                        Quarantine Centers
                    </a>
                </li>
                <!-- <li class="app-sidebar__heading">Quarantine Room</li> -->
                <li>
                    <a href="{{route('rooms.index')}}" class="{{Request::path() === 'rooms' ? 'mm-active' : '' }}" >
                        <i class="metismenu-icon pe-7s-plugin"></i>
                        Quarantine Rooms
                    </a>
                </li>
                <li class="app-sidebar__heading">Quarantine (Patient)</li>
                <li>
                    <a href="{{route('quarantineUser.index')}}" class="{{Request::path() === 'quarantine_patients' ? 'mm-active' : '' }}" >
                        <i class="metismenu-icon pe-7s-user"></i>
                        Quarantine (Patients)
                    </a>
                </li>
                <li class="app-sidebar__heading">Quarantine (Volunteer)</li>
                <li>
                    <a href="{{route('volunteers.index')}}" class="{{Request::path() === 'volunteers' ? 'mm-active' : '' }}" >
                        <i class="metismenu-icon pe-7s-users"></i>
                        Quarantine (Volunteers)
                    </a>
                </li>
                <li>
                    <a href="{{route('ondutys.create')}}" class="{{Request::path() === 'ondutys/create' ? 'mm-active' : '' }}" >
                        <i class="metismenu-icon pe-7s-paperclip"></i>
                        Add OnDuty
                    </a>
                </li>
                <li>
                    <a href="{{route('dutyOffs.create')}}" class="{{Request::path() === 'dutyoffs/create' ? 'mm-active' : '' }}" >
                        <i class="metismenu-icon pe-7s-paperclip"></i>
                        Add Duty Off
                    </a>
                </li>

                <!-- <li class="app-sidebar__heading">Region / State</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-diamond"></i>
                        Region / State
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="elements-buttons-standard.html">
                                <i class="metismenu-icon"></i>
                                Region / State List
                            </a>
                        </li>
                        <li>
                            <a href="elements-dropdowns.html">
                                <i class="metismenu-icon">
                                </i>Dropdowns
                            </a>
                        </li>
                        <li>
                            <a href="elements-icons.html">
                                <i class="metismenu-icon">
                                </i>Icons
                            </a>
                        </li>
                        <li>
                            <a href="elements-badges-labels.html">
                                <i class="metismenu-icon">
                                </i>Badges
                            </a>
                        </li>
                        <li>
                            <a href="elements-cards.html">
                                <i class="metismenu-icon">
                                </i>Cards
                            </a>
                        </li>
                        <li>
                            <a href="elements-list-group.html">
                                <i class="metismenu-icon">
                                </i>List Groups
                            </a>
                        </li>
                        <li>
                            <a href="elements-navigation.html">
                                <i class="metismenu-icon">
                                </i>Navigation Menus
                            </a>
                        </li>
                        <li>
                            <a href="elements-utilities.html">
                                <i class="metismenu-icon">
                                </i>Utilities
                            </a>
                        </li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </div>
</div>
