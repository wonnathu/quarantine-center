@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Edit OnDuty For Volunteers
        </div>
    </div>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
  @endif

  @if (session('store'))
    <div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
      <strong>{{session('store')}}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif


      <div class="col-md-8 offset-md-2  border p-5 mt-5 mb-5 bg-light">
          <form action="{{route('ondutys.update', $onduty->id)}}" method="post" >
            @csrf
            @method('patch')
            <div class="form-row">
            <div class="col-md-6 mb-3">
              <label for="entry_date">Entry Date *</label>
              <input type="date" class="form-control @error('entry_date') border border-danger @enderror" id="entry_date" name="entry_date" value="{{$onduty->entry_date->format('Y-m-d')}}">
              @error("entry_date")
              <small  class="mt-3 form-text text-danger">
                {{ $message }}
              </small>
              @enderror
            </div>
            <div class="col-md-6 mb-3">
              <label for="leave_date">Leave Date *</label>
              <input type="date" class="form-control @error('leave_date') border border-danger @enderror" id="leave_date" name="leave_date" value="{{$onduty->leave_date->format('Y-m-d')}}">
              @error("leave_date")
              <small  class="mt-3 form-text text-danger">
                {{ $message }}
              </small>
              @enderror
            </div>
          </div>

          <div class="form-group">
            <label for="volunteer_id">Choose Volunteer</label>
            <select class="form-control" name="volunteer_id">
              @foreach($volunteers as $volunteer)
              <option value="{{$volunteer->id}}"
                @if($volunteer->id == $onduty->volunteer_id)
                  selected
                @endif
              >
                {{$volunteer->volunteer_name}}
              </option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">
            Edit OnDuty
          </button>
        </form>
      </div>

@endsection
