@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
                OnDuty List
        </div>
        <div class="page-title-actions">
            <a href="{{route('volunteers.create')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-note font-size-xl "> </i> Add New Duty
            </a>
        </div>
    </div>
</div>

@if (session('store'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('store')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('update'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('update')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('delete'))
<div class="alert alert-danger mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('delete')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


  <table id="volunteer" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>Profile</th>
        <th>Volunteer Name</th>
        <th>On Duty Duration</th>
        <th>Duty Off Duration</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($volunteers as $volunteer)
        <tr>
            <td>{{$volunteer->id}}</td>
            <td>
                <img src="{{url('storage/'.str_replace('public/', '', $volunteer->profile_image))}}" alt="{{$volunteer->volunteer_name}}" style="width: 70px; height: 70px">
            </td>
            <td>{{$volunteer->volunteer_name}}</td>
            <td>
              @foreach($volunteer->ondutys as $volunteerOnDuty)
                <div>
                  {{$volunteerOnDuty->entry_date->format('d/m/Y')}} -
                  {{$volunteerOnDuty->leave_date->format('d/m/Y')}}
                </div>
              @endforeach
            </td>
            <td>
              @foreach($volunteer->dutyoffs as $volunteerDutyOff)
                <div>
                  {{$volunteerDutyOff->start_date->format('d/m/Y')}} -
                  {{$volunteerDutyOff->end_date->format('d/m/Y')}}
                </div>
              @endforeach
            </td>
            <td>
                <a href="{{route('volunteers.show', $volunteer->id)}}" class="ml-3 text-info font-size-lg" title="Show Patient">
                    <i class="pe-7s-monitor"> </i>
                </a>
                <!-- <a href="{{route('volunteers.edit', $volunteer->id)}}" class="ml-3 text-info font-size-lg" title="Edit Patient">
                    <i class="pe-7s-note2"> </i>
                </a>
                <form action="{{ route('volunteers.destroy', $volunteer->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Patient">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form> -->
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>Profile</th>
        <th>Volunteer Name</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>



@endsection

@section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#volunteer').DataTable();
    });
  </script>
  @endsection
