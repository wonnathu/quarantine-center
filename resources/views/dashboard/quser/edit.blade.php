@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
  <div class="page-title-wrapper">
      <div class="page-title-heading">
          Edit Patient
      </div>
      <div class="page-title-actions">
          <a href="{{route('quarantineUser.index')}}" class="mr-3 btn btn-primary text-light" >
              <i class="pe-7s-back font-size-xl "> </i> Back To Patients List
          </a>
      </div>
  </div>
</div>

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

      <div class="col-md-8 offset-md-2  border p-5 mt-5 bg-light">
          <form action="{{route('quarantineUser.update', $quarantineUser->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
          <div class="form-group">
            <label for="quser_name" class="h5">Patient Name *</label>
            <input type="text" class="form-control @error('quser_name') border border-danger @enderror" id="quser_name" placeholder="Enter Patient Name" name="quser_name" value="{{$quarantineUser->quser_name}}">
            @error("quser_name")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <img src="{{url('storage/'.str_replace('public/', '', $quarantineUser->profile_image))}}" alt="{{$quarantineUser->quser_name}}" style="width: 70px; height: 70px; display: block; border: 1px solid gray;">
          <label for="image" class="h5 mt-2">Profile Image</label>
          <div class="custom-file mb-3">
            <input type="file" class="custom-file-input" id="validatedCustomFile" name="image">
            <label class="custom-file-label" for="validatedCustomFile">Choose profile...</label>
          </div>

          <div class="form-group">
            <label for="phone" class="h5">Phone No.</label>
            <input type="text" class="form-control @error('phone') border border-danger @enderror" id="phone" placeholder="Enter Phone No." name="phone" value="{{$quarantineUser->phone}}">
            @error("phone")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-group">
            <label for="address" class="h5">Address *</label>
            <textarea name="address" rows="7" class="form-control @error('address') border border-danger @enderror" id="address" placeholder="Enter address">{{$quarantineUser->address}}</textarea>
            @error("address")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-row">
            <div class="col-md-6 mb-3">
              <label for="entry_date">Entry Date *</label>
              <input type="date" class="form-control @error('entry_date') border border-danger @enderror" id="entry_date" name="entry_date" value="{{$quarantineUser->entry_date->format('Y-m-d')}}">
              @error("entry_date")
              <small  class="mt-3 form-text text-danger">
                {{ $message }}
              </small>
              @enderror
            </div>
            <div class="col-md-6 mb-3">
              <label for="leave_date">Leave Date *</label>
              <input type="date" class="form-control @error('leave_date') border border-danger @enderror" id="leave_date" name="leave_date" value="{{$quarantineUser->leave_date->format('Y-m-d')}}">
              @error("leave_date")
              <small  class="mt-3 form-text text-danger">
                {{ $message }}
              </small>
              @enderror
            </div>
          </div>

          <div class="form-group">
            <label for="remark" class="h5">Remark</label>
            <textarea name="remark" rows="7" class="form-control @error('remark') border border-danger @enderror" id="remark" placeholder="Enter remark">{{$quarantineUser->remark}}</textarea>

            @error("remark")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>


          <div>
            <patient-edit-component :centers="{{$centers}}" :rooms="{{$rooms}}" :centerid="{{$quarantineUser->center_id}}" :roomid="{{$quarantineUser->room_id}}" />
          </div>
          @error("room_id")
            <small  class="mt-3 mb-3 form-text text-danger">
              Choose Center and Rooms
            </small>
          @enderror

          <!-- <div class="form-group">
            <label for="center_id">Choose Quarantine Center</label>
            <select class="form-control" >
              <option selected>Choose...</option>
              @foreach($centers as $center)
              <option onclick="test()" value="{{$center->id}}">
                {{$center->center_name}}
              </option>
              @endforeach
            </select>
          </div> -->

          <button type="submit" class="btn btn-primary">
            Update Patient
          </button>
        </form>
      </div>

@endsection
