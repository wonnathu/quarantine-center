@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>
                Quarantine (Patient) Lists
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{route('quarantineUser.create')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-note font-size-xl "> </i> Add New Parient
            </a>
        </div>
    </div>
</div>

@if (session('store'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('store')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('update'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('update')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('delete'))
<div class="alert alert-danger mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('delete')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<table id="quarantine_patient" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>Profile</th>
        <th>Patients Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Entry Date</th>
        <th>Leave Date</th>
        <th>Remark</th>
        <th>Room No.</th>
        <th>Center</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($quarantine_patients as $quarantine_patient)
        <tr>
            <td>{{$quarantine_patient->id}}</td>
            <td>
                <img src="{{url('storage/'.str_replace('public/', '', $quarantine_patient->profile_image))}}" alt="{{$quarantine_patient->quser_name}}" style="width: 70px; height: 70px">
            </td>
            <td>{{$quarantine_patient->quser_name}}</td>
            <td>{{$quarantine_patient->phone}}</td>
            <td>{{$quarantine_patient->address}}</td>
            <td>{{$quarantine_patient->entry_date->format('d/m/Y')}}</td>
            <td>{{$quarantine_patient->leave_date->format('d/m/Y')}}</td>
            <td>{{$quarantine_patient->remark}}</td>
            <td>{{$quarantine_patient->room->room_name}}</td>
            <td>{{$quarantine_patient->center->center_name}}</td>
            <td>
                <a href="{{route('quarantineUser.show', $quarantine_patient->id)}}" class="ml-3 text-info h4" title="Show Patient">
                    <i class="pe-7s-monitor"> </i>
                </a> <br>
                <a href="{{route('quarantineUser.edit', $quarantine_patient->id)}}" class="ml-3 text-info font-size-lg" title="Edit Patient">
                    <i class="pe-7s-note2"> </i>
                </a> <br>
                <form action="{{ route('quarantineUser.destroy', $quarantine_patient->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Patient">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>Profile</th>
        <th>Patients Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Entry Date</th>
        <th>Leave Date</th>
        <th>Remark</th>
        <th>Room No.</th>
        <th>Center</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>


  @endsection
  @section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#quarantine_patient').DataTable();
    });
  </script>
  @endsection
