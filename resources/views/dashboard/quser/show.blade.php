@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
  <div class="page-title-wrapper">
      <div class="page-title-heading">
          Quarantine (Patient) Profile
      </div>
      <!-- <div class="page-title-actions">
          <a href="{{route('quarantineUser.index')}}" class="mr-3 btn btn-primary text-light" >
              <i class="pe-7s-back font-size-xl "> </i> Back To Patient List
          </a>
      </div> -->
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-4">
    <div class="card">
      <img src="{{url('storage/'.str_replace('public/', '', $quarantineUser->profile_image))}}" class="card-img-top" alt="{{$quarantineUser->volunteer_name}}">
    </div>
  </div>
  <div class="col-md-8 col-sm-8">
  <div class="main-card mb-3 card">
      <div class="card-header">
          About - {{$quarantineUser->quser_name}}
          <div class="btn-actions-pane-right">

            <!-- @if(now()->diffInDays($quarantineUser->entry_date) ==0)
                <span class="text-success h6">
                    This Patiends entry date is today.
                </span>
            @else
                <span class="text-success h3">
                    {{now()->diffInDays($quarantineUser->entry_date)}} Days
                </span>
            @endif -->
            <span class="text-success h3">
                ({{now()->diffInDays($quarantineUser->leave_date) + 1}}) Left Days
            </span>
          </div>
      </div>
      <div class="table-responsive">
          <table class="align-middle mb-0 table table-borderless table-striped table-hover">
              <thead>

              </thead>
              <tbody>
              <tr>
                    <th>ID</th>
                    <td>#{{$quarantineUser->id}}</t>
                </tr>
              <tr>
                  <th>Patient Name</th>
                  <td>{{$quarantineUser->quser_name}}</t>
              </tr>
              <tr>
                  <th>Phone No.</td>
                  <td>{{$quarantineUser->phone}}</t>
              </tr>
              <tr>
                  <th>Address</td>
                  <td>{{$quarantineUser->address}}</t>
              </tr>
              <tr>
                  <th>Entry Date</td>
                  <td>{{$quarantineUser->entry_date->format('d / m / Y')}}</t>
              </tr>
              <tr>
                  <th>Leave Date</td>
                  <td>{{$quarantineUser->leave_date->format('d / m / Y')}}</t>
              </tr>
              <tr>
                  <th>Remark</td>
                  <td>{{$quarantineUser->remark}}</t>
              </tr>
              <tr>
                  <th>Room No.</td>
                  <td>{{$quarantineUser->room->room_name}}</t>
              </tr>
              <tr>
                  <th>Quarantine Center</td>
                  <td>{{$quarantineUser->center->center_name}}</t>
              </tr>
              <tr>
                  <th>Township</td>
                  <td>{{$quarantineUser->center->township->township_name}}</t>
              </tr>
              <tr>
                  <th>Region / State</td>
                  <td>{{$quarantineUser->center->township->state->state_name}}</t>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="d-block text-center card-footer">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem sed cum illo quae, asperiores quam voluptates quo assumenda iure eos ducimus sapiente placeat similique molestias in provident error ipsum dicta?</p>
      </div>
  </div>
  </div>
</div>

@endsection
