@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Add New Room
        </div>
        <div class="page-title-actions">
            <a href="{{route('rooms.index')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-back font-size-xl "> </i> Back To Room
            </a>
        </div>
    </div>
  </div>

      <div class="col-md-8 offset-md-2  border p-5 mt-5 bg-light">
          <form action="{{route('rooms.store')}}" method="post">
            @csrf
          <div class="form-group">
            <label for="room_name" class="sr-only">Add New Room</label>
            <input type="text" class="form-control @error('room_name') border border-danger @enderror" id="room_name" placeholder="Enter Room No." name="room_name">
            @error("room_name")
            <small id="emailHelp" class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-group">
            <label for="center_id">Choose Quarantine Center</label>
            <select class="form-control" id="center_id" name="center_id">
              @foreach($centers as $center)
              <option value="{{$center->id}}">{{$center->center_name}}</option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">
            Add New Room
          </button>
        </form>
      </div>

@endsection
