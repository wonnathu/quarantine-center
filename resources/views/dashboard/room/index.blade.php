@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>
                Room Lists
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{route('rooms.create')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-note font-size-xl "> </i> Add New Room
            </a>
        </div>
    </div>
</div>

@if (session('store'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('store')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('update'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('update')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('delete'))
<div class="alert alert-danger mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('delete')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<table id="room" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th style="width: 70px">ID</th>
        <th>Room No.</th>
        <th>Quarantine Centers</th>
        <th>township</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($rooms as $room)
        <tr>
            <th>{{$room->id}}</th>
            <td>{{$room->room_name}}</td>
            <td>{{$room->center->center_name}}</td>
            <td>{{$room->center->township->township_name}}</td>
            <td>
                <a href="{{route('rooms.edit', $room->id)}}" class="ml-3 text-info font-size-lg" title="Edit Room">
                    <i class="pe-7s-note2"> </i>
                </a>
                <form action="{{ route('rooms.destroy', $room->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Room">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th style="width: 70px">ID</th>
        <th>Room No.</th>
        <th>Quarantine room</th>
        <th>township</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>

  @section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#room').DataTable();
    });
  </script>
  @endsection

  @endsection
