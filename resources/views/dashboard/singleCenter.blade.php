@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
              {{$center->center_name}} Center
        </div>
        <div class="page-title-actions">
            <a href="{{route('patient', $center->id)}}" class="mr-3 btn btn-primary text-light" >
               Patient Lists
            </a>
        </div>
    </div>
    <a href="{{route('township', $center->township_id)}}" class="btn btn-link"><i class="fas fa-reply"></i> Back</a>
</div>

<div class="mb-5">
    <h2 class="h4 text-center">
        <span class="text-success">({{count($rooms)}})</span> <span class="text-secondary">Rooms</span> |
        <span class="text-success">({{count($patients)}})</span> <span class="text-secondary">Patients</span> |
        <span class="text-success">({{count($volunteers)}})</span> <span class="text-secondary">Volunteers</span>
    </h2>
    <div class="row mt-5">
        @foreach($rooms as $room)
        <div class="col-md-3 mb-4">
            <div class="card border-info h-100">
                <div class="card-body p-0">
                    <a href="{{route('patient', $center->id)}}?room_id={{$room->id}}" class="h4 m-0 p-4 d-block text-center" style="text-decoration: none;">
                        <i class="metismenu-icon h3 text-info pe-7s-plugin"></i>
                        <h4 class="text-muted h5">
                            {{$room->room_name}}
                        </h4>
                        <p class="h6">
                            @if(count($room->quarantineUsers) ==0)
                                <span class="text-danger">No Patients</span>
                            @else
                                <span class="text-success">{{count($room->quarantineUsers)}} Patients </span>
                            @endif
                        </p>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="divider mt-5 mb-5"></div>

<div class="mb-5">
    <h2 class="h4 text-info mb-4">
        Volunteers List
    </h2>

    <table id="volunteer" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th>No.</th>
        <th>Profile</th>
        <th>Volunteer Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Center</th>
        <th>Show</th>
      </tr>
    </thead>
    <tbody>
        @foreach($volunteers as $index=>$volunteer)
        <tr>
            <td>{{++$index}}</td>
            <td>
                <img src="{{url('storage/'.str_replace('public/', '', $volunteer->profile_image))}}" alt="{{$volunteer->volunteer_name}}" style="width: 70px; height: 70px">
            </td>
            <td>{{$volunteer->volunteer_name}}</td>
            <td>{{$volunteer->phone}}</td>
            <td>{{$volunteer->address}}</td>
            <td>{{$volunteer->center->center_name}}</td>
            <td>
                <a href="{{route('volunteers.show', $volunteer->id)}}" class="ml-3 text-info h4" title="Show Patient">
                    <i class="pe-7s-monitor"> </i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>No.</th>
        <th>Profile</th>
        <th>Volunteer Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Center</th>
        <th>Show</th>
      </tr>
    </tfoot>
  </table>
</div>
@endsection
@section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#volunteer').DataTable();
    });
  </script>
  @endsection
