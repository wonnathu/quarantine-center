@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>
                {{$center->center_name}}'s Patients List
            </div>
        </div>
    </div>
    <a href="{{route('center', $center)}}" class="btn btn-link"><i class="fas fa-reply"></i> Back</a>
</div>

<table id="quarantine_patient" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th>No.</th>
        <th>Profile</th>
        <th>Patients Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Entry Date</th>
        <th>Leave Date</th>
        <th>Remark</th>
        <th>Room No.</th>
        <th>Center</th>
        <th>Show</th>
      </tr>
    </thead>
    <tbody>
        @foreach($quarantine_patients as $index=>$quarantine_patient)
        <tr>
            <td>{{++$index}}</td>
            <td>
                <img src="{{url('storage/'.str_replace('public/', '', $quarantine_patient->profile_image))}}" alt="{{$quarantine_patient->quser_name}}" style="width: 70px; height: 70px">
            </td>
            <td>{{$quarantine_patient->quser_name}}</td>
            <td>{{$quarantine_patient->phone}}</td>
            <td>{{$quarantine_patient->address}}</td>
            <td>{{$quarantine_patient->entry_date->format('d/m/Y')}}</td>
            <td>{{$quarantine_patient->leave_date->format('d/m/Y')}}</td>
            <td>{{$quarantine_patient->remark}}</td>
            <td>{{$quarantine_patient->room->room_name}}</td>
            <td>{{$quarantine_patient->center->center_name}}</td>
            <td>
                <a href="{{route('quarantineUser.show', $quarantine_patient->id)}}" class="ml-3 text-info h4" title="Show Patient">
                    <i class="pe-7s-monitor"> </i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>No.</th>
        <th>Profile</th>
        <th>Patients Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Entry Date</th>
        <th>Leave Date</th>
        <th>Remark</th>
        <th>Room No.</th>
        <th>Center</th>
        <th>Show</th>
      </tr>
    </tfoot>
  </table>


  @endsection
  @section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#quarantine_patient').DataTable();
    });
  </script>
  @endsection
