@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Edit Region / State
        </div>
        <div class="page-title-actions">
            <a href="{{route('states.index')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-back font-size-xl "> </i> Back To Region / State
            </a>
        </div>
    </div>
  </div>

      <div class="col-md-8 offset-md-2  border p-5 mt-5 bg-light">
          <form action="{{route('states.update', $state->id)}}" method="post">
            @csrf
            @method('patch')
          <div class="form-group">
            <label for="statename" class="sr-only">Add New Region / State</label>
            <input type="text" class="form-control @error('state_name') border border-danger @enderror" id="statename" placeholder="Enter Region or State Name" name="state_name" value="{{$state->state_name}}">
            @error('state_name')
            <small id="emailHelp" class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <button type="submit" class="btn btn-primary">
            Update Region / State
          </button>
        </form>
      </div>

@endsection
