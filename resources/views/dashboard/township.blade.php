@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{$state->state_name}} Region / State
        </div>
    </div>
    <a href="{{route('dashboard')}}" class="btn btn-link"><i class="fas fa-reply"></i> Back</a>
</div>

<div class="mb-5">
    <h2 class="h4 text-center">
        Townships
    </h2>
    <div class="row mt-5">
        @foreach($townships as $township)
        <div class="col-md-3 mb-4">
            <div class="card border-info h-100">
                <div class="card-body p-0">
                    <a href="{{route('township', $township->id)}}" class="h4 m-0 p-4 d-block text-center" style="text-decoration: none;">
                        <i class="metismenu-icon h3 text-info pe-7s-culture"></i>
                        <h4 class="text-muted h5">
                            {{$township->township_name}}
                        </h4>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</div>

@endsection
