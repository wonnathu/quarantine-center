@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Add New Township
        </div>
        <div class="page-title-actions">
            <a href="{{route('townships.index')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-back font-size-xl "> </i> Back To Township
            </a>
        </div>
    </div>
  </div>

      <div class="col-md-8 offset-md-2  border p-5 mt-5 bg-light">
          <form action="{{route('townships.store')}}" method="post">
            @csrf
          <div class="form-group">
            <label for="township_name" class="sr-only">Add New Township</label>
            <input type="text" class="form-control @error('township_name') border border-danger @enderror" id="township_name" placeholder="Enter Township Name" name="township_name">
            @error('township_name')
            <small id="emailHelp" class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-group">
            <label for="state_id">Choose Region / State</label>
            <select class="form-control" id="state_id" name="state_id">
              @foreach($states as $state)
              <option value="{{$state->id}}">{{$state->state_name}}</option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">
            Add New Township
          </button>
        </form>
      </div>

@endsection
