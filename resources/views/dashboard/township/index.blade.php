@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">

            <div>
                Township Lists
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{route('townships.create')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-note font-size-xl "> </i> Add New Township
            </a>
        </div>
    </div>
</div>

@if (session('store'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('store')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('update'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('update')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('delete'))
<div class="alert alert-danger mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('delete')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<table id="township" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th style="width: 100px">ID</th>
        <th>Township Names</th>
        <th>State / Region</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($townships as $township)
        <tr>
            <td>{{$township->id}}</td>
            <td>{{$township->township_name}}</td>
            <td>{{$township->state->state_name}}</td>
            <td>
                <a href="{{route('townships.edit', $township->id)}}" class="ml-3 text-info font-size-lg" title="Edit township">
                    <i class="pe-7s-note2"> </i>
                </a>
                <form action="{{ route('townships.destroy', $township->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Township">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
      <tr>
        <th style="width: 100px">ID</th>
        <th>Township Names</th>
        <th>township / Region</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>

  @section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#township').DataTable();
    });
  </script>
  @endsection

  @endsection
