@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            Edit Volunteer
        </div>
        <div class="page-title-actions">
            <a href="{{route('volunteers.index')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-back font-size-xl "> </i> Back To Volunteer List
            </a>
        </div>
    </div>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

      <div class="col-md-8 offset-md-2  border p-5 mt-5 mb-5 bg-light">
          <form action="{{route('volunteers.update', $volunteer->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
          <div class="form-group">
            <label for="volunteer_name" class="h5">Volunteer Name *</label>
            <input type="text" class="form-control @error('volunteer_name') border border-danger @enderror" id="volunteer_name" placeholder="Enter Volunteer Name" name="volunteer_name" value="{{$volunteer->volunteer_name}}">
            @error("volunteer_name")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <img src="{{url('storage/'.str_replace('public/', '', $volunteer->profile_image))}}" alt="{{$volunteer->volunteer_name}}" style="width: 70px; height: 70px; display: block; border: 1px solid gray;">
          <label for="image" class="h5 mt-2">Profile Image</label>
          <div class="custom-file mb-3">
            <input type="file" class="custom-file-input" id="validatedCustomFile" name="image">
            <label class="custom-file-label" for="validatedCustomFile">Choose profile...</label>
          </div>

          <div class="form-group">
            <label for="phone" class="h5">Phone No.</label>
            <input type="text" class="form-control @error('phone') border border-danger @enderror" id="phone" placeholder="Enter Phone No." name="phone" value="{{$volunteer->phone}}">
            @error("phone")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-group">
            <label for="address" class="h5">Address *</label>
            <textarea name="address" rows="7" class="form-control @error('address') border border-danger @enderror" id="address" placeholder="Enter address">{{$volunteer->address}}</textarea>
            @error("address")
            <small  class="mt-3 form-text text-danger">
              {{ $message }}
            </small>
            @enderror
          </div>

          <div class="form-group">
            <label for="center_id">Choose Quarantine Center</label>
            <select class="form-control" name="center_id">
              @foreach($centers as $center)
              <option onclick="test()" value="{{$center->id}}"
                @if($center->id === $volunteer->center_id)
                  selected
                @endif
              >
                {{$center->center_name}}
              </option>
              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">
            Edit Volunteer
          </button>
        </form>
      </div>

@endsection
