
@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>
                Quarantine (Volunteer) Lists
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{route('volunteers.create')}}" class="mr-3 btn btn-primary text-light" >
                <i class="pe-7s-note font-size-xl "> </i> Add New Volunteer
            </a>
        </div>
    </div>
</div>

@if (session('store'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('store')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('update'))
<div class="alert alert-success mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('update')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (session('delete'))
<div class="alert alert-danger mb-5 alert-dismissible fade show" role="alert">
  <strong>{{session('delete')}}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

  <table id="volunteer" class="table table-striped table-bordered mt-4 mb-4" style="width:100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>Profile</th>
        <th>Volunteer Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Center</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($volunteers as $volunteer)
        <tr>
            <td>{{$volunteer->id}}</td>
            <td>
                <img src="{{url('storage/'.str_replace('public/', '', $volunteer->profile_image))}}" alt="{{$volunteer->volunteer_name}}" style="width: 70px; height: 70px">
            </td>
            <td>{{$volunteer->volunteer_name}}</td>
            <td>{{$volunteer->phone}}</td>
            <td>{{$volunteer->address}}</td>
            <td>{{$volunteer->center->center_name}}</td>
            <td>
                <a href="{{route('volunteers.show', $volunteer->id)}}" class="ml-3 text-info h4" title="Show Patient">
                    <i class="pe-7s-monitor"> </i>
                </a> <br>
                <a href="{{route('volunteers.edit', $volunteer->id)}}" class="ml-3 text-info font-size-lg" title="Edit Patient">
                    <i class="pe-7s-note2"> </i>
                </a> <br>
                <form action="{{ route('volunteers.destroy', $volunteer->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Patient">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    <tr>
    <th>ID</th>
        <th>Profile</th>
        <th>Volunteer Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Center</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>


  @endsection
  @section('script')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#volunteer').DataTable();
    });
  </script>
  @endsection
