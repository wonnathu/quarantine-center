@extends('dashboard.layouts.master')

@section('title', "Dashboard")

@section('content')
<div class="app-page-title">
  <div class="page-title-wrapper">
      <div class="page-title-heading">
          Volunteer Profile
      </div>
      <!-- <div class="page-title-actions">
          <a href="{{route('volunteers.index')}}" class="mr-3 btn btn-primary text-light" >
              <i class="pe-7s-back font-size-xl "> </i> Back To Volunteer List
          </a>
      </div> -->
  </div>
</div>

<div class="row">
  <div class="col-md-3 col-sm-4">
    <div class="card">
      <img src="{{url('storage/'.str_replace('public/', '', $volunteer->profile_image))}}" class="card-img-top" alt="{{$volunteer->volunteer_name}}">
    </div>
  </div>
  <div class="col-md-8 col-sm-8">
  <div class="main-card mb-3 card">
      <div class="card-header">
          About - {{$volunteer->volunteer_name}}
          <div class="btn-actions-pane-right">
              <div role="group" class="btn-group-sm btn-group">
                  <a href="#onduty" class="btn btn-success">Onduty</a>
                  <a href="#dutyoff" class="btn btn-danger">Duty Off</a>
              </div>
          </div>
      </div>
      <div class="table-responsive">
          <table class="align-middle mb-0 table table-borderless table-striped table-hover">
              <thead>

              </thead>
              <tbody>
              <tr>
                    <th>ID</th>
                    <td>#{{$volunteer->id}}</t>
                </tr>
              <tr>
                  <th>Volunteer Name</th>
                  <td>{{$volunteer->volunteer_name}}</t>
              </tr>
              <tr>
                  <th>Phone No.</td>
                  <td>{{$volunteer->phone}}</t>
              </tr>
              <tr>
                  <th>Address</td>
                  <td>{{$volunteer->address}}</t>
              </tr>
              <tr>
                  <th>Quarantine Center</td>
                  <td>{{$volunteer->center->center_name}}</t>
              </tr>
              <tr>
                  <th>Township</td>
                  <td>{{$volunteer->center->township->township_name}}</t>
              </tr>
              <tr>
                  <th>Region / State</td>
                  <td>{{$volunteer->center->township->state->state_name}}</t>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="d-block text-center card-footer">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem sed cum illo quae, asperiores quam voluptates quo assumenda iure eos ducimus sapiente placeat similique molestias in provident error ipsum dicta?</p>
      </div>
  </div>
  </div>
</div>

<div class="divider mt-5 mb-5"></div>

<div id="onduty"></div>
<div id="dutyoff"></div>

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active btn btn-success mr-2" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Onduty Record</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link btn btn-danger ml-2" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Duty Off Record</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
  <table id="volunteer" class="table table-striped table-bordered mt-5 mb-5" style="width:100%">
    <thead>
      <tr class="bg-success text-light">
        <th>ID</th>
        <th>On Duty Duration</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($volunteer->ondutys as $volunteerOnduty)
        <tr>
            <td>
                {{$volunteerOnduty->id}}
            </td>
            <td>
                <span>
                    {{$volunteerOnduty->entry_date->format('d/m/Y')}} -
                    {{$volunteerOnduty->leave_date->format('d/m/Y')}}
                </span>
            </td>
            <td>
                <a href="{{route('ondutys.edit', $volunteerOnduty->id)}}" class="ml-3 text-info font-size-lg" title="Edit Duty On">
                    <i class="pe-7s-note2"> </i>
                </a>
                <form action="{{ route('ondutys.destroy', $volunteerOnduty->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Duty On">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
            </td>
        </tr>
        @endforeach

    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>On Duty Duration</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>

  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <table id="volunteer" class="table table-striped table-bordered mt-5 mb-5" style="width:100%">
        <thead>
        <tr class="bg-danger text-light">
            <th>ID</th>
            <th>Duty Off Duration</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($volunteer->dutyoffs as $volunteerDutyOff)
            <tr>
                <td>
                    {{$volunteerDutyOff->id}}
                </td>
                <td>
                    <span>
                        {{$volunteerDutyOff->start_date->format('d/m/Y')}} -
                        {{$volunteerDutyOff->end_date->format('d/m/Y')}}
                    </span>
                </td>
                <td>
                    <a href="{{route('dutyOffs.edit', $volunteerDutyOff->id)}}" class="ml-3 text-info font-size-lg" title="Edit Duty Off">
                    <i class="pe-7s-note2"> </i>
                    </a>
                    <form action="{{ route('dutyOffs.destroy', $volunteerDutyOff->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-link" title="Delete Duty Off">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Duty Off Duration</th>
            <th>Action</th>
        </tr>
        </tfoot>
    </table>

  </div>
</div>

@endsection
