<?php

use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::redirect('/', '/dashboard');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('/states', 'StateController');
    Route::resource('/townships', 'TownshipController');
    Route::resource('/centers', 'CenterController');
    Route::resource('/rooms', 'RoomController');
    Route::resource('/quarantineUser', 'QuarantineUserController');
    Route::resource('/volunteers', 'VolunteerController');
    Route::resource('/ondutys', 'OndutyController');
    Route::resource('/dutyOffs', 'DutyOffController');

    Route::get('/dashboard/state/{state}', 'DashboardController@state')->name('state');
    Route::get('/dashboard/township/{township}', 'DashboardController@township')->name('township');
    Route::get('/dashboard/center/{center}', 'DashboardController@center')->name('center');

    Route::get('/dashboard/center/{center}/patient', 'DashboardController@patient')->name('patient');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', function () {
    $volunteers = App\Volunteer::unique();

    dd($volunteers);

});
